const bsConfig = {
  files: [
    `${__dirname}/public/css/critical.css`
  ],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`public/*.html`]
    }
  }],
  server: `public`
};

require(`browser-sync`).init(bsConfig);
