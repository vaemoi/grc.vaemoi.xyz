const robotstxt = require(`generate-robotstxt`).default;

robotstxt({
  policy: [
    {
      userAgent: `Googlebot`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    },
    {
      userAgent: `Twitterbot`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    },
    {
      userAgent: `*`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    }
  ],
  sitemap: `https://grc.vaemoi.co/sitemap.xml`,
  host: `https://grc.vaemoi.co`
}).then((content) => {
  require(`fs`).writeFileSync(`./public/robots.txt`, content);
});
